module.exports = function(deps){
    var mongoose = deps.mongoose,
        Schema = mongoose.Schema;
        
    var ForensiqScores = new Schema({
        ip          : String,
        riskScore   : Number,
        nonSuspect  : String,
        date        : { type: Date, default: Date.now }
        
    });
    
    ForensiqScores.index({ ip: 1 });
    ForensiqScores.index({ ip: 1, date: 1 });
    
    
    var model = mongoose.model(deps.config.getProperty('mongo_collection'), ForensiqScores);
    
    return model;
};
