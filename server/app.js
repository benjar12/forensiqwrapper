var deps = require('./lib/deps'),
    app = deps.express();

var methods = ['get', 'post', 'delete'];

for(i=0;i<methods.length;i++){
    
    var method = methods[i];
    app[method]('*', deps.router(method));

}

var server = app.listen(deps.config.getProperty('server_port'), function () {

    var host = server.address().address;
    var port = server.address().port;

    deps.logger.info('App listening at http://%s:%s', host, port);

});