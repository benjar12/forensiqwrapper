module.exports = function(deps){
    
    function get(req, res){
        
        //set vars
        var ip = req.query.ip;
        var asyncDone = 0;
        var records = [];
        var rExists = false;
        
        //validate
        if(!ip){
            
            deps.logger.error('no ip provied: %s', ip);
            
            res.setJsonRespAttr('status', 400);
            res.setJsonRespAttr('error', 'no ip address provided!');
            res.jsonRender();
            
            return;
        
        }
        
        //set forensiq params
        var params = {
            host: deps.config.getProperty('forensiq_host'),
            values:[
                {key: "ck", value: deps.config.getProperty('forensiq_key')},
                {key: "output", value:"json"},
                {key: "ip", value: ip}
                ],
            path: deps.config.getProperty('forensiq_path'),
            port: 80
        };
        
        //make http request
        deps.httpreq.get(params, httpcb);
        
        function httpcb(error, data){
            
            if(error){
                
                final(error);
                
            }else{
            
                data['date'] = new Date();
                data['ip'] = ip;
                
                var r = new deps.models.forensiq(data);
                r.save(mongocb);
            
            }
            
        }
        
        function mongocb(error, data){
            
            if(error){
                
                final(error);
                
            }else{
                
                deps.models.forensiq.find(
                    {ip:ip},
                    null, 
                    {sort: {date: 1}}, 
                    final);
            
            }
            
        }
        
        function final(error, data){
            
            if(error){
                
                deps.logger.error(error);
                res.setJsonRespAttr('status', 500);
            
            }
            
            if(data){ records = data; }
                
            //send response
            res.setJsonRespAttr('records', records);
            res.jsonRender();
                
        }
        
        function logErrors(err){
            if (err) deps.logger.error(err);
        }
    
    }
    
    var forensiq = new deps.Route('forensiq');
    
    forensiq.registerMethod('get', get);
    
    return forensiq;
    
};
