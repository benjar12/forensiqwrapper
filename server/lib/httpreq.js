module.exports = function(deps) {

    function httpget(params, callback) {

        //set vars
        var port = (params.port || 80 ), 
            host = params.host, 
            path = params.path, 
            queryArray = params.values, // [{key:STRING, value:STRING}]
            qlength = queryArray.length;

        path += (qlength > 0 ) ? "?" : "";

        for ( i = 0; i < qlength; i++) {

            var c = queryArray[i];

            path += c.key + "=" + encodeURIComponent(c.value);

            if (!(qlength - 1 == i)) {

                path += "&";

            }

        }

        var options = {
            host : host,
            path : path,
            port : port,
            method : 'GET'
        };

        var request = deps.http.request(options, function(response) {
            var body = "";

            response.on('data', function(data) {

                body += data;

            });

            response.on('end', function() {
                
                deps.logger.info('[%s][%s]: Successful request', options.host, options.path);
                callback( false, JSON.parse(body) );

            });
        });

        request.on('error', function(e) {

            deps.logger.error(e);
            callback(e, false);

        });

        request.end();
    }

    return {
        get : httpget
    };

};
