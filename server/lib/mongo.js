module.exports = function(deps){
    
    deps.mongoose.connect('mongodb://' + deps.config.getProperty('mongo_host') + '/' + deps.config.getProperty('mongo_database'), loadModels);

    function loadModels(err){
        
        //throw mongo connect errors
        if(err) {
            deps.logger.error(err);
            throw err;
        }
        
        //create model object
        deps.models = {
            forensiq: require('../models/forensiq')(deps)
        };
        
        deps.logger.info('Connected to mongo');
        
    };
    
};
