module.exports = function(deps){
    
    var res = deps.express.response;
    
    
    res.JsonResp = function(response){
        this.response = response;
        this.jsonrobj = {
            error: '',
            status: 200
        };
    };
    
    res.JsonResp.prototype.setJsonRespAttr = function(attr, value){
        this.jsonrobj[attr] = value;
    };
    
    res.JsonResp.prototype.jsonRender = function() {
        this.response.header("Content-Type", "application/json");
        this.response.status(this.jsonrobj.status).send( this.jsonrobj );
    };
};
