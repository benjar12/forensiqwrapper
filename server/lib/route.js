module.exports = function(deps){
    
    function Route(name){
        
        this.name = name;
        
        this.methods = {};
        this.avMethods = ['post', 'get', 'delete'];
        
        //helper functions not implemented yet
        this.helpers = [];
    }
    
    Route.prototype.registerMethod = function(method, methodFunction){
        
        if(this.avMethods.indexOf(method)>-1) {
            
            this.methods[method] = methodFunction;
            deps.logger.info('[%s]: registered %s method.', this.name, method);
        
        }else{
            
            deps.logger.error('[%s]: %s is an invalid method.', this.name, method);
            
        }
    };
    
    Route.prototype.getMethod = function(method){
        
        return (this.methods[method]||this.defaultMethod);
        
    };
    
    Route.prototype.defaultMethod = function(req, res){
        
        deps.logger.error('unsupported method');
        res.setJsonRespAttr('status', 400);
        res.setJsonRespAttr('error', 'unsupported method');
        res.jsonRender();
        
    };
    
    return Route;
    
};
