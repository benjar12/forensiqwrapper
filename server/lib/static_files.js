module.exports = function(deps) {

    var fs = deps.fs;

    function StaticFiles() {
        this.cachedFiles = {};
    }


    StaticFiles.prototype.getFile = function(file, callback) {
        
        var that = this;
        
        if (!deps.config.getProperty('server_static_caching') || !this.cachedFiles[file]) {
            
            var f = new StaticFile();
            
            that.cachedFiles[file] = f;
            
            try {

                fs.readFile('../client/' + file, 'utf8', function(err, data) {

                    if (err) {
                        
                        deps.logger.error(err);
                        f.setData("<body>" + err + "</body>");
                        f.setStatus(500);

                    } else {

                        f.setData(data);

                    }
                    
                    callcb();

                });

            } catch(err) {
                
                deps.logger.error(err);
                f.setData("<body>" + err + "</body>"); 
                f.setStatus(500);
                
                callcb();

            }
            
        }else{
            
            callcb();
        
        }
        
        function callcb(){
            (callback) ? callback( that.cachedFiles[file] ) : false;
        }
        
    };
    
    function StaticFile(){
        this.data = '';
        this.status = 200;
    }
    
    StaticFile.prototype.setData = function(d){
        this.data = d;
    };
    
    StaticFile.prototype.getData = function(){
        return this.data;
    };
    
    StaticFile.prototype.setStatus = function(s){
        this.status = s;
    };
    
    StaticFile.prototype.getStatus = function(){
        return (this.status||200);
    };
    
    
    return new StaticFiles();
};
