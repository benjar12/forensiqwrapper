var express = require('express'),
    fs = require('fs'),
    http = require('http'),
    winston = require('winston'),
    mongoose = require('mongoose');

//package deps
var deps        = {};
deps.fs         = fs;
deps.http       = http;
deps.express    = express;
deps.winston    = winston;
deps.mongoose   = mongoose;
deps.env        = require('./env');
deps.config     = require('./config')(deps);
deps.logger     = require('./logger')(deps);
deps.mongo      = require('./mongo')(deps);
deps.httpreq    = require('./httpreq')(deps);
deps.Route      = require('./route')(deps);
deps.router     = require('./router')(deps);
deps.static     = require('./static_files')(deps);

//load any prototypes
require('./json_resp')(deps);
require('./static_resp')(deps);

//return
module.exports = deps;
