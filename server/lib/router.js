module.exports = function(deps){
    
    //{forensiq:{get:function, post:function, delete:function}}
    var apiRoutes = {};
    
    return function(reqType){
        
        return function (req, res) {
            
            //log request
            deps.logger.info('[%s]: %s', reqType, req.url);
            
            //split url
            var urlParts = req.url.split('?')[0].replace(deps.config.getProperty('server_base'), '').split('/');
            
            //get context
            var context = urlParts.shift();
            
            //load index
            if(context == ''){
                
                var fileName='index.html';
                res.staticRender(fileName);
            
            //load static file       
            }else if(context == 'client'){
                
                var fileName = urlParts.join('/');
                res.staticRender(fileName);
                
            //pass to api
            }else if(context == 'api'){
                
                var fileName = urlParts.join('/');
                
                if(!apiRoutes[fileName]){
                    apiRoutes[fileName] = require('../app/'+fileName)(deps);
                }
                
                var response = new res.JsonResp(res);
                
                apiRoutes[fileName].getMethod(reqType)(req, response);
                
            //else error
            }else{
                
                var response = new res.JsonResp(res);
                
                deps.logger.error('invalid context: %s', context);
                response.setJsonRespAttr('status', 400);
                response.setJsonRespAttr('error', 'not a vaild route');
                response.jsonRender();
                
            }
            
        };
    };
    
};
