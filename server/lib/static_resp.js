module.exports = function(deps){
    
    var res = deps.express.response;
    
    var contentTypeCache = {};
    
    res.staticRender = function(fileName) {
        
        var that = this;
        
        deps.static.getFile(fileName, cb);
        
        function cb(dat){
            
            if(!contentTypeCache[fileName]){
                
                if(fileName.indexOf('.html') > -1){
                    contentTypeCache[fileName] = 'text/html';
                }else if(fileName.indexOf('.js') > -1){
                    contentTypeCache[fileName] = 'text/javascript';
                }else if(fileName.indexOf('.css') > -1){
                    contentTypeCache[fileName] = 'text/css';
                }else{
                    contentTypeCache[fileName] = 'text/plain';
                }
                
            }
            
            that.header("Content-Type", contentTypeCache[fileName]);
            that.status(dat.getStatus()).send(dat.getData());
            
        }
    };
};
