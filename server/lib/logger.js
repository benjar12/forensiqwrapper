module.exports = function(deps){
    
    var errorLog = ( deps.config.getProperty('log_error') || 'error.log' );
    var infoLog = ( deps.config.getProperty('log_info') || 'info.log' );
    
    var logger = new (deps.winston.Logger)({
      transports: [
        new (deps.winston.transports.File)({
          name: 'info-file',
          filename: infoLog,
          level: 'info'
        }),
        new (deps.winston.transports.File)({
          name: 'error-file',
          filename: errorLog,
          level: 'error'
        }),
        new (deps.winston.transports.Console)()
      ]
    });
    
    logger.info('logger Loaded');
    
    return logger;
    
};
