module.exports = function(deps){
    
    var Config = function(){};
    
    Config.prototype.loadProperites = function(path){
        
        var defaults = require(path);
        for (var k in defaults) { this.setProperty(k, defaults[k]); }
    
    };
    
    Config.prototype.loadDefaults = function(){
        
        this.loadProperites('../etc/defaults.json');
    
    };
    
    Config.prototype.loadOverrides = function(){
        
        var overrideConfigPath = deps.env('FORENSIQ_CONF');
        
        if(overrideConfigPath){ this.loadProperites(overrideConfigPath); }
        
    };
    
    Config.prototype.getProperty = function(p){
        return ( this[p] || false );
    };
    
    Config.prototype.setProperty = function(p, v){
        this[p] = v;
    };
    
    var conf = new Config();
    
    conf.loadDefaults();
    conf.loadOverrides();
    
    return conf;
    
};
