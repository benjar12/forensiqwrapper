function Results(){
    this.results = [];
}
Results.prototype.appendResult = function(r){
    this.results.push(r);
};
Results.prototype.toHtml = function(){
    var html = "";
    for(i=0;i<this.results.length;i++){
        html += this.results[i].toHtml();
    }
    return html;
};


function Result(ip, date, score){
    this.ip = ip;
    this.date = date;
    this.score = score;
};
Result.prototype.toHtml = function(){
    
    return '<div class="alert" role="alert">'+
    '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>'+
    '<span class="sr-only">Ip:</span>'+
    this.ip +
    '<br>' +
    '<span class="sr-only">Date:</span>'+
    this.date +
    '<br>' +
    '<span class="sr-only">Score:</span>'+
    this.score+
    '</div>';
    
};

function ErrorResult(ip){
    this.ip = ip;
}
ErrorResult.prototype.toHtml = function(){
    return '<div class="alert alert-danger" role="alert">'+
    '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>'+
    '<span class="sr-only">Error:</span>'+
    'invalid ip: '+ this.ip+
    '</div>';
};

function checkIp(ip, callback){
    $.get( "/forensiq/api/forensiq?ip="+ip, function( data ) {
      callback( data );
    });
}

$(document).click(function(e){
    var target = e.target;
    var data = target.dataset;
    
    if(data.type == "single"){
        (function(){
            var val = $('#singleiptextbox').val();
            var count = val.split('.').length;
            var results = new Results();
            
            if(count != 4){
                //err
               results.appendResult(new ErrorResult(val));
               render();
               
            }else{
                //checkip
                checkIp(val, function(data){
                    
                    var records = data['records'];
                    
                    for(i=records.length-1;i!=-1;i--){
                        var curr = records[i];
                        results.appendResult(new Result(val, curr['date'], curr['riskScore']));
                    }
                    
                    render();
                });
            }
            function render(){
                $('#singleresultholder').empty().html( results.toHtml() );
            }
        })();
        
        
        
    }else if(data.type == "multi"){
        (function(){
            
            var val = $('#multiiptextbox').val();
            
            var ips = val.split('\n');
            var results = new Results();
            
            if(!val){
                
               //err
               results.appendResult(new ErrorResult(val));
               render();
            
            }else{
                
                for(i=0;i<ips.length;i++){
                    var cur = ips[i].trim();
                    process(cur);
                    
                    
                }
                
                function process(ip){
                        checkIp(cur, function(data){
                        
                            var records = data['records'];
                            
                            var curr = records.pop();
                            results.appendResult(new Result(ip, curr['date'], curr['riskScore']));
                            
                            
                            render();
                        });
                    };
                
            }
            
            function render(){
                $('#multiresultholder').empty().html( results.toHtml() );
            }
        })();
    }
    
});
