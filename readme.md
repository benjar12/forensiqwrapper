1. Overview
This code base was meant to create a simple wrapper around the forensiq scoring api. We can use this tool to test ip addresses over time. It stores results in mongo, using the mongoose orm. This is a node based project. I used this none production system as a chance to experiment with new ways of doing things. 
2. How It works
app.js creates an express application that routes all requests to lib/router.js. router.js dynamically loads static files and api methods as they are needed. any url that begins with /client/ will be directed to the client folder and output the file. Any url with /api/ will search for an api method file. The api method files create a instance of lib/route.js and provides a function for get | post | delete
3. Dependancies
node.js
express
mongoose
winston
4. Setup
a. install node
b. git clone ...
c. cd {repopath}/server && npm install
d. create an override properties file with the foresiq key in it
d.1 vim some.json
d.2 insert {"forensiq_key":"{key}"}
e. export FORENSIQ_CONF=/path/to/some.json
f. node app.js
g. go to http://localhost:3000/